use gettextrs::*;
use gtk4::prelude::*;
use gtk4::{Application, ApplicationWindow, Box, Button, Orientation};
use std::process::Command;

mod config;

fn main() {
    gtk4::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("bar-launcher", config::LOCALEDIR);
    textdomain("bar-launcher");

    let app = Application::builder().application_id("org.example.App").build();

    app.connect_activate(move |app| {
        let window = ApplicationWindow::builder()
            .title("Beyond All Reason - Launcher")
            .application(app)
            .build();

        let update_button = Button::builder()
            .label("Update")
            .margin_top(12)
            .margin_bottom(12)
            .margin_start(12)
            .margin_end(12)
            .build();

        update_button.connect_clicked(move |button| {
            println!("Update clicked");
        });

        let launch_button = Button::builder()
            .label("Launch")
            .margin_top(12)
            .margin_bottom(12)
            .margin_start(12)
            .margin_end(12)
            .build();

        let button_box = Box::builder().build();
        button_box.set_orientation(Orientation::Vertical);
        button_box.append(&update_button);
        button_box.append(&launch_button);

        launch_button.connect_clicked(move |button| {
            println!("Launch");
            let mut list_dir = Command::new("ls");

            list_dir.status().expect("failed to execute process");
        });

        window.set_child(Some(&button_box));
        window.set_size_request(600, 200);

        window.present();
    });

    let ret = app.run();
    std::process::exit(ret);
}
